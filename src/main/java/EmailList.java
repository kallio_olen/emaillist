import java.util.TreeSet;

public class EmailList {
    private final TreeSet<String> emailList = new TreeSet();

    public void add(String input) {
        if (input.matches("[a-zA-Z]+@[a-zA-Z]+\\.[a-zA-Z]+"))
            emailList.add(input.toLowerCase());
           }

            public TreeSet<String> getSortedEmails() {

        return emailList;

    }
}